package com.dev9.Compass.Steps;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;





public class CompassIntegral extends PageObject{

	private static WebDriver driver;
    private static String baseUrl;
	
    
    @Step
	public static void OpenURL() {
    	System.setProperty("webdriver.chrome.driver","lib/chromedriver.exe");
    	driver = new ChromeDriver();
        baseUrl = "http://test.vmaxcompass.com/compass/Login.aspx";

       	driver.get(baseUrl + " " );
       	driver.manage().window().maximize();
       System.out.println("Ingreso a Compass");
		
	}

    @Step
	public static void Login() throws InterruptedException {
	       Thread.sleep(4000);
	       driver.findElement(By.id("txtLoginName")).sendKeys("vancouver");
	       driver.findElement(By.id("txtPassword")).sendKeys("vancouver!0");
	       driver.findElement(By.xpath("//div/button")).click();
	        
	       //Scenario
	       Thread.sleep(4000);
	       driver.findElement(By.xpath("//*[@id=\"ctlScenarios_lstScenarios\"]/option[15]")).click();
	       driver.findElement(By.id("ctlScenarios_btnOpen")).click();
		
	}

    @Step
	public static void IntroLabel(String label) throws InterruptedException {
    	Thread.sleep(4000);

    	if (label.equals("Runs"))
    		{
        		driver.findElement(By.id("tabContext_tablist_tpgRoutes")).click();
    		}

    	
    	System.out.println("Entry to " + label);
		
	}

    @Step
	public static void SearchRun(String ruta) throws InterruptedException {
    	Thread.sleep(100);

    	driver.findElement(By.id("ctlContextMgr_ctlRoutes_txtRouteName")).click();
    	driver.findElement(By.id("ctlContextMgr_ctlRoutes_txtRouteName")).sendKeys(ruta);
    	
    	driver.findElement(By.id("ctlContextMgr_ctlRoutes_btnFind")).click();
    	
    	
    	Thread.sleep(4000);
    	String Result = driver.findElement(By.xpath("//*[@id=\"gview_ctlContextMgr_ctlRoutes_tblRoutes\"]/div[1]/span")).getText();
    	System.err.println(Result);
    	
    	
    	Assert.assertEquals("Runs Search Results (10)", Result );
    	//assertThat(Result.isEqualTo("Runs Search Results (10)"));
		
	}

    @Step
	public static void Quit() throws InterruptedException {
    	Thread.sleep(4000);
    	driver.quit();
		
	}

    @Step
	public static void OpenDatatools() throws Throwable {
    	
    	String oldTab = driver.getWindowHandle();

    	driver.findElement(By.id("divDataTools_label")).click();
    	Thread.sleep(4000);
    	
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldTab);
        driver.switchTo().window(newTab.get(0));
        
    	driver.findElement(By.xpath("//li[9]/a")).click();
	}

}
