package com.dev9.Compass.Steps;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import net.thucydides.core.annotations.Step;


public class RestOrganizations {

	@Produces("application/json")
	@Consumes(MediaType.APPLICATION_JSON)	

	
	@Step
//	public static void GetService(String[] args) {
	public static void GetServiceOrganizations() {

	 
	
	Client cliente = ClientBuilder.newClient();
	RestGetOrganizations f = cliente.target("https://dev-datatools-api.vmaxcompass.com/api/Organizations/Get/ORG-VANCOUVER")
	.request(MediaType.APPLICATION_JSON_TYPE).get(RestGetOrganizations.class);
	 
		
	System.out.println(f.getcode());
	System.out.println(f.getname());
	System.out.println(f.getconnectionString());
	System.out.println(f.getdbPort());
	System.out.println(f.getendpoint());
	System.out.println(f.getendpointUsername());
	System.out.println(f.getendpointPassword());
	System.out.println(f.gettimeZone());
	System.out.println(f.getdbName());
	System.out.println(f.getdbServer());
	System.out.println(f.getdbUserName());
	System.out.println(f.getdbPassword());


	}
	
}
