package com.dev9.Compass.Steps;


import net.thucydides.core.annotations.*;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

public class CompassSteps {

    @Steps
    CompassIntegral compasIntegral;
    

    @Step
    @Title ("Entry in the Application")
    public void ingresar_al_aplicativo_vmaxcompass() throws Throwable  {
     	
    	CompassIntegral.OpenURL();
    	CompassIntegral.Login();

    }
    
    @Step
    @Title ("Entry in the Label")
    public void ingresar_en_la_pestana(String Label) throws Throwable {
    	CompassIntegral.IntroLabel(Label);

    }

    @Step
    @Title ("Search in Run")
    public void buscar_la_ruta(String ruta) throws Throwable {
    	
    	CompassIntegral.SearchRun(ruta);
    	CompassIntegral.OpenDatatools();
    	
    	CompassIntegral.Quit();

        }
}