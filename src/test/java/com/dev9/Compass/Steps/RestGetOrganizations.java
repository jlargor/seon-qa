package com.dev9.Compass.Steps;


import java.io.Serializable;
 
public class RestGetOrganizations implements Serializable {
 

	
	private static final long serialVersionUID = 1L;

	private String code;
	private String name;
	private String connectionString;
	private String dbPort;
	private String endpoint;
	private String endpointUsername;
	private String endpointPassword;
	private String timeZone;
	private String dbName;
	private String dbServer;
	private String dbUserName;
	private String dbPassword;

	
	public String getcode(){
	return code;
	}
	public void setcode(String code) {
	this.code = code;
	}
	
	public String getname(){
	return name;
	}
	public void setname(String name) {
	this.name = name;
	}
	
	public String getconnectionString(){
	return connectionString;
	}
	public void setconnectionString(String connectionString) {
	this.connectionString = connectionString;
	}
	
	public String getdbPort(){
	return dbPort;
	}
	public void setdbPort(String dbPort) {
	this.dbPort = dbPort;
	}
	
	public String getendpoint(){
	return endpoint;
	}
	public void setendpoint(String endpoint) {
	this.endpoint = endpoint;
	}
	
	public String getendpointUsername(){
	return endpointUsername;
	}
	public void setendpointUsername(String endpointUsername) {
	this.endpointUsername = endpointUsername;
	}
	
	public String getendpointPassword(){
	return endpointPassword;
	}
	public void setendpointPassword(String endpointPassword) {
	this.endpointPassword = endpointPassword;
	}
	
	public String gettimeZone(){
	return timeZone;
	}
	public void settimeZone(String timeZone) {
	this.timeZone = timeZone;
	}
	
	public String getdbName(){
	return dbName;
	}
	public void setdbName(String dbName) {
	this.dbName = dbName;
	}
	
	public String getdbServer(){
	return dbServer;
	}
	public void setdbServer(String dbServer) {
	this.dbServer = dbServer;
	}
	
	public String getdbUserName(){
	return dbUserName;
	}
	public void setdbUserName(String dbUserName) {
	this.dbUserName = dbUserName;
	}
	
	public String getdbPassword(){
	return dbPassword;
	}
	public void setdbPassword(String dbPassword) {
	this.dbPassword = dbPassword;
	}
	


}



