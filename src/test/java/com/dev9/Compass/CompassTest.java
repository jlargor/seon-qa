package com.dev9.Compass;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.dev9.Compass.Steps.CompassSteps;
import com.dev9.Compass.Steps.RestOrganizations;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Steps;
import net.serenitybdd.junit.runners.SerenityRunner; 


@RunWith(SerenityRunner.class)
public class CompassTest {

	
    @Managed(driver = "chrome")                                                   
    WebDriver driver;
	
	@Steps
	CompassSteps CompassSteps;
	RestOrganizations Organizations;
	

	
	@Test
	public void Integral_Compass() {
		
		try {
			CompassSteps.ingresar_al_aplicativo_vmaxcompass();
			CompassSteps.ingresar_en_la_pestana("Runs");
			CompassSteps.buscar_la_ruta("test");
					
		} catch (Throwable e) {
			e.printStackTrace();
		}

		
		System.err.println("Proceso Finalizado Integral compassTest");
		
	}
	
	
	@Test
	public void ServiceAPIOrganization() {
		RestOrganizations.GetServiceOrganizations();
	}
	
	
}
